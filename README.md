# Jasmine --- DMO Application on miRNA-seq data #

Application of Jasmine to small RNA sequencing dataset

To demonstrate Jasmine usage, we investigated a small RNA dataset that compared healthy and cancer mice at various time points. 

This repository provides all file for reproduce analysis for Jasmine's application on dataset from [Nogales-Cadenas 2016](https://breast-cancer-research.biomedcentral.com/articles/10.1186/s13058-016-0735-z).

### NOTICE###
Jasmine currently only tested on MacOS and Linux. No test on Windows yet.

Currently, this repository contains `trimmomatic-0.36.jar` and processed data from [Nogales-Cadenas 2016](https://breast-cancer-research.biom) for quick reproduce analysis. We will remove them if any copyright issue arose.  

------------------------
### Reproduce case study ###
Open terminal, and type following `git clone git@bitbucket.org:bipous/jasmine_casestudy.git`. Then `cd` to local folder for case study `jasmine_casestudy`

Some result folders are also provided in compressed ZIP files.

------------------------
### Dataset retrieve ###

A text file contains SRA accession number `jasmine_caseStudy_SRP076396_SRA_acc.tsv` is available for downloading dataset.

Sample information provided in  `jasmine_caseStudy_SRP076396.tsv`

After downloaded all sequencing dataset, save in fastq format. (If you download as .sra files, please use `fastq-dump` to convert dataset into fastq format)

Optional, if you are concerning the disk space, you can use `pigz` to compress those downloaded files. i.e., fastq files are deposited in the folder `readsFasta`
```
cd readsFasta
for file in ./*.fastq; do pigz ${file};done
```

Alternative, we provided processed data in the folder `CollapsedReads`. Annotation data from miRBase and MirGeneDB are also included in this repo. Users can directly start from the step of building reference and mapping.

------------------------
### Configuration ###
In this repo, an example configuration is provided `jasmine_caseStudy_SRP076396_confg.xml`. Users need to use **YOUR OWN PATH ANF DIRECTORY**. 


------------------------
### Source Code ###
The source code of Jasmine is deposited at [bitbucket](https://bitbucket.org/bipous/jasmine/src/master/)
User can comile from source or use the [compiled jar file](https://bitbucket.org/bipous/jasmine/src/master/compiled_jar/)

------------------------
### Usage ###
In terminal, `cd ` to the cloned local folder, then `java -jar Jasmine-1.1.jar jasmine_config.xml`

------------------------
### Please cite ###

Jasmine


